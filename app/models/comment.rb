class Comment < ActiveRecord::Base
  belongs_to :post
  validates_presence_of :body
  validates :post, :presence => true
end
